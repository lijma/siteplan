package org.lijma.demo.siteplan.config;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.lijma.demo.siteplan.config.impl.FileRoutesLoader;

import java.io.File;
import java.io.StringReader;

public class TestFileRoutesLoader {

    private FileRoutesLoader routesLoader;

    @Before
    public void setup(){
        routesLoader = new FileRoutesLoader();
    }

    @Test
    public void FileRoutesLoaderShouldLoadRouteInfoAsDefaultMapping() throws LoadFailedException {
        Assert.assertEquals(routesLoader.loadRouteInfo().size(),9);
    }

    @Test
    public void FileRoutesLoaderShouldReturnNullWhenNoContentConfigured () throws LoadFailedException {
        routesLoader.setReader(new StringReader(""));
        Assert.assertNull(routesLoader.loadRouteInfo());
    }

    @Test
    public void FileRoutesLoaderShouldReturnSizeOfRoutesConfigured() throws LoadFailedException {
        routesLoader.setReader(new StringReader("AB5, BC4, CD8, DC8"));
        Assert.assertEquals(routesLoader.loadRouteInfo().size(),4);
    }

    @Test( expected = LoadFailedException.class)
    public void FileRoutesLoaderShouldReturnExceptionFormatWrong() throws LoadFailedException {
        routesLoader.setReader(new StringReader("5, B4, CD8, D8"));
        Assert.assertEquals(routesLoader.loadRouteInfo().size(),4);
    }

}
