package org.lijma.demo.siteplan.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.lijma.demo.siteplan.core.impl.ManagerImpl;
import java.util.Arrays;

public class TestStopSearchImpl {

    private StopSearcher stopSearcher;
    private ManagerImpl manager;

    @Before
    public void setup(){
        manager = new ManagerImpl();
        stopSearcher = manager;
    }

    @Test
    public void shouldReturnErrorMsgWhenFindWithUnknownStop(){
        String msg = null;
        try{
            manager.addRoute("A","B",1);
            stopSearcher.findDistanceOfRoutes(Arrays.asList("C","D"));
        }catch(NoSuchRouteException e){
            msg = e.getMessage();
        }
        Assert.assertEquals(msg,"NO SUCH ROUTE");
    }

    @Test
    public void shouldReturnRightWeight() throws NoSuchRouteException {
        manager.addRoute("A","B",1);
        Assert.assertEquals(stopSearcher.findDistanceOfRoutes(Arrays.asList("A","B")),1);
    }

    @Test
    public void shouldReturnMutipleLevelWeight() throws NoSuchRouteException {
        manager.addRoute("A","B",5);
        manager.addRoute("B","C",2);
        manager.addRoute("C","D",7);
        manager.addRoute("B","D",5);
        Assert.assertEquals(stopSearcher.findDistanceOfRoutes(Arrays.asList("A","B","C","D")),14);
        Assert.assertEquals(stopSearcher.findDistanceOfRoutes(Arrays.asList("A","B","D")),10);
    }

    @Test
    public void shouldReturnRightTripsNumber() throws NoSuchRouteException {
        manager.addRoute("A","B",5);
        manager.addRoute("A","C",2);
        manager.addRoute("B","C",7);
        Assert.assertEquals(stopSearcher.getTripsByMaximumSteps("A","C",3).size(),2);
        Assert.assertEquals(stopSearcher.getTripsByMaximumSteps("B","C",3).size(),1);
        Assert.assertEquals(stopSearcher.getTripsByMaximumSteps("A","C",1).size(),1);
    }

    @Test
    public void shouldReturnZeroWhenTripNotFound() throws NoSuchRouteException {
        manager.addRoute("A","B",5);
        Assert.assertEquals(stopSearcher.getTripsByMaximumSteps("A","D",4).size(),0);
    }


    @Test
    public void shouldReturnRightTripsNumberWithRequiredStops() throws NoSuchRouteException {
        manager.addRoute("A","B",5);
        manager.addRoute("B","C",2);
        manager.addRoute("A","D",1);
        manager.addRoute("D","C",1);
        manager.addRoute("A","C",7);

        Assert.assertEquals(stopSearcher.getTripsByRequiredStops("A","C",2).size(),2);
        Assert.assertEquals(stopSearcher.getTripsByRequiredStops("A","C",1).size(),1);
    }

    @Test
    public void shouldReturnShortestRoute(){
        manager.addRoute("A","B",2);
        manager.addRoute("B","C",2);
        manager.addRoute("A","C",5);
        Assert.assertEquals(stopSearcher.getShortestRouteAndDistance("A","C").getTotalWeight(),4);
    }

    @Test
    public void shouldReturnMatchedRoutes(){
        manager.addRoute("A","B",2);
        manager.addRoute("B","C",2);
        manager.addRoute("A","C",5);
        Assert.assertEquals(stopSearcher.getMatchedRouteAndDistance("A","C",6).size(),2);
    }

}
