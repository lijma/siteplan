package org.lijma.demo.siteplan.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.lijma.demo.siteplan.core.impl.ManagerImpl;

public class TestStopManagerImpl {

    private StopManager stopManager;

    @Before
    public void setup(){
        stopManager = new ManagerImpl();
    }

    @Test
    public void StopManagerShouldAddAStop(){
        stopManager.addStop("A");
    }

    @Test
    public void StopManagerShouldReturnAllStops(){
        Assert.assertEquals(stopManager.getStops().length,0);
        stopManager.addStop("A");
        stopManager.addStop("B");
        Assert.assertEquals(stopManager.getStops().length,2 );
    }

    @Test
    public void StopManagerShouldNotAddSameStop(){
        Assert.assertEquals(stopManager.getStops().length,0);
        stopManager.addStop("A");
        stopManager.addStop("B");
        stopManager.addStop("B");
        Assert.assertEquals(stopManager.getStops().length,2 );
    }

    @Test
    public void StopManagerShouldReturnNullWhenTargetStopNotExist(){
        Assert.assertEquals(stopManager.getRoutesByStop("notExist"),null);
    }

    @Test
    public void StopManagerShouldReturnEmptyWhenNotRoutesAdded(){
        stopManager.addStop("A");
        Assert.assertEquals(stopManager.getRoutesByStop("A").size(),0);
    }

    @Test
    public void StopManagerShouldAddRoute(){
        stopManager.addRoute("A","B",1);
        Assert.assertEquals(stopManager.getRoutesByStop("A").size(),1);
    }

    @Test
    public void StopManagerShouldAddMultipleRoutes(){
        stopManager.addRoute("A","B",1);
        stopManager.addRoute("A","C",1);
        Assert.assertEquals(stopManager.getRoutesByStop("A").size(),2);
    }

    @Test
    public void AddSameRouteWillLeadTheFormerOneOverwritten(){
        stopManager.addRoute("A","B",1);
        stopManager.addRoute("A","B",2);
        Assert.assertEquals(stopManager.getRoutesByStop("A").size(),1);
        Assert.assertEquals(stopManager.getRoutesByStop("A").get(0).getWeight(),2);
    }

    @Test
    public void StopManagerShouldAddMultipleLvelRoutes(){
        stopManager.addRoute("A","B",1);
        stopManager.addRoute("B","C",2);
        stopManager.addRoute("C","D",2);
        stopManager.addRoute("A","C",4);
        Assert.assertEquals(stopManager.getRoutesByStop("B").size(),1);
        Assert.assertEquals(stopManager.getRoutesByStop("C").size(),1);
        Assert.assertEquals(stopManager.getRoutesByStop("D").size(),0);
        Assert.assertEquals(stopManager.getRoutesByStop("A").size(),2);
    }


}
