package org.lijma.demo.siteplan.config;

public class LoadFailedException extends Exception {

    public LoadFailedException(String message) {
        super(message);
    }

    public LoadFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoadFailedException(Throwable cause) {
        super(cause);
    }
}
