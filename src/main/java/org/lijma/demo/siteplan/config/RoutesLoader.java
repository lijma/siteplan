package org.lijma.demo.siteplan.config;

import java.util.List;

public interface RoutesLoader {

    List<RouteInfo> loadRouteInfo() throws LoadFailedException;

}
