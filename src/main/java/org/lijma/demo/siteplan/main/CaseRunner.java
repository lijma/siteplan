package org.lijma.demo.siteplan.main;

import org.lijma.demo.siteplan.core.NoSuchRouteException;

public interface CaseRunner {

    void runCase(int caseNumber, Case testCase);

    interface Case{
        public int invoke() throws Exception;
    }
}
