package org.lijma.demo.siteplan.main.impl;

import org.lijma.demo.siteplan.main.CaseRunner;

public class CaseRunnerImpl implements CaseRunner {
    public void runCase(int caseNumber,Case testCase) {
        StringBuilder message = new StringBuilder();
        message.append("Output #").append(caseNumber).append(": ");
        try{
            int result = testCase.invoke();
            message.append(result);
        }catch (Exception e){
            message.append(e.getMessage());
        }
        System.out.println(message);
    }
}
