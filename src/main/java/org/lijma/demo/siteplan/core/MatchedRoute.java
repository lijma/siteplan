package org.lijma.demo.siteplan.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MatchedRoute implements Serializable {

    private List<String> routeLine;
    private int totalWeight;

    public MatchedRoute() {
        this.routeLine = new LinkedList<String>();
        this.totalWeight = 0;
    }

    public List<String> getRouteLine() {

        return routeLine;
    }

    public void setRouteLine(List<String> routeLine) {
        this.routeLine = routeLine;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }
}
