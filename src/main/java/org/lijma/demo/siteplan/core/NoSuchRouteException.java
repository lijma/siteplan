package org.lijma.demo.siteplan.core;

public class NoSuchRouteException extends Exception {

    public NoSuchRouteException() {
        super("NO SUCH ROUTE");
    }

}
