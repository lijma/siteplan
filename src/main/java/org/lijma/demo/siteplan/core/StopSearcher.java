package org.lijma.demo.siteplan.core;

import javax.naming.Reference;
import java.util.List;
import java.util.Map;

public interface StopSearcher {

    public int findDistanceOfRoutes(List<String> stopNames) throws NoSuchRouteException;

    public List<List<String>> getTripsByMaximumSteps(String startStop, String endStop, int maximumStops);

    public List<List<String>> getTripsByRequiredStops(String startStop, String endStop, int requiredStops);

    public MatchedRoute getShortestRouteAndDistance(String startStop, String endStop);

    public List<MatchedRoute> getMatchedRouteAndDistance(String startStop, String endStop, int maximumWeight);
}
