package org.lijma.demo.siteplan.core.bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class Stop {

    private static Logger LOG = Logger.getLogger(Stop.class.getName());

    private String stopName;
    private boolean visited;
    private List<StopRoute> stopRoutes;

    public Stop(String stopName){
        this.stopName = stopName;
        stopRoutes = new ArrayList<StopRoute>();
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public List<StopRoute> getStopRoutes() {
        return Collections.unmodifiableList(stopRoutes);
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    /**
     * check if stopRoute exist before, if so, weight will be overwrite
     * @param stopRoute
     */
    public void addRoute(StopRoute stopRoute){
        for(StopRoute iterator : stopRoutes){
            Stop addedRoute = stopRoute.getStop();
            Stop checkRoute = iterator.getStop();
            if(addedRoute.getStopName().equals(checkRoute.getStopName())){
                LOG.warning("stopRoute existed before, weight will be overwrite by current value: "+ stopRoute.getWeight());
                iterator.setWeight(stopRoute.getWeight());
                return;
            }
        }
        stopRoutes.add(stopRoute);
    }
}
