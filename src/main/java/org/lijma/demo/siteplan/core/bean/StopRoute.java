package org.lijma.demo.siteplan.core.bean;

public class StopRoute {

    private int weight;
    private Stop stop;

    public StopRoute(int weight, Stop stopRoute){
        this.weight = weight;
        this.stop = stopRoute;
    }

    public StopRoute(){}

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Stop getStop() {
        return stop;
    }

    public void setStop(Stop stop) {
        this.stop = stop;
    }
}
