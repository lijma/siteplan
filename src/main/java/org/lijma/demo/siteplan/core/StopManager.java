package org.lijma.demo.siteplan.core;

import org.lijma.demo.siteplan.core.bean.StopRoute;

import java.util.List;

public interface StopManager {
    void addStop(String stopName);
    String[] getStops();
    void addRoute(String stopName, String toStop, int weight);
    List<StopRoute> getRoutesByStop(String stopName);
}
